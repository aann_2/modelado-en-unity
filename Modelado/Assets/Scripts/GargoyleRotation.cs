using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GargoyleRotation : MonoBehaviour
{
    private int direction;
    public float maxRotation;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        direction = +1;
    }

    // Update is called once per frame
    void Update()
    {
        //Controlar la rotaci�n
        if ((transform.localRotation.eulerAngles.y <= 1) && (direction < 0))
        {
            direction = 1;
        }
        if ((transform.localRotation.eulerAngles.y >= maxRotation) && (direction > 0))
        {
            direction = -1;
        }
        
        transform.Rotate(0, direction * speed * Time.deltaTime, 0);

    }
}
